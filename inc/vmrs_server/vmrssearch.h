#ifndef _VMRSSEARCH_H_
#define _VMRSSEARCH_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <vmrs/vmrssizes.h>
#include <vmrs/vmrs.h>
#include <vmrs/lookuptable.h>
#include <vmrs/postinglist.h>
#include <vmrs/arraylist.h>
#include <vmrs/qset.h>
#include <vmrs/trapdoor.h>

using namespace std;
using namespace chrono;

using doc_list = unordered_map<ulong,float>;
using res_list = vector<pair<unsigned long, float>>;

class ArrayData {
 public:
  Zr yic;
  unsigned long fid;
  float score;
  bool beta;
};

class VmrsSearch {
 public:
  VmrsSearch(const VMRSSizes& vs, const string& pairing_file, const string& arrays_file, const string& lt_file, const string& qset_file, const string& res_file);

  void Search(const string& file_path);
  res_list calculateResults(const doc_list& list, int k);
  void save_results(const vector<res_list>& res);

  ArrayData get_array_data(size_t addr, size_t c, string& fi_k3_w1);
  float find_token (string &w, string &token, ArrayData &ar);

  VMRSSizes vs;

  //LookupTable lt;
  pLookupTable lt;
  QSet qs;
  ArrayList al;
  string res_file;
  bool is_verbose () const;
  void set_verbose (bool verbose);

  int get_repetitions () const;
  void set_repetitions (int repetitions);
 private:
  Pairing* pairing;

  int repetitions;
  bool verbose;
  bool res_all_files;
 public:
  bool is_res_all_files () const;
  void set_res_all_files (bool res_all_files);
 private:
  void logmsg(const string& msg);
  high_resolution_clock::time_point start;
};

#endif //_VMRSSEARCH_H_
