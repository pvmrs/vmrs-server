#include <iostream>
#include <ios>
#include <vmrs/argparse.h>
#include <vmrs/configparams.h>
#include "vmrssearch.h"

using namespace std;


int main (int argc, char* argv[])
{
  ArgumentParser parser ("Argument parser for vmrs_server");
  parser.add_argument("-c", "configuration file", false);
  parser.add_argument("-r", "repetitions for each search", false);
  parser.add_argument("-a", "all files", false);

  try
    {
      parser.parse(argc, argv);
    }
  catch (const ArgumentParser::ArgumentNotFound& ex)
    {
      cout << "Error processing arguments: " << ex.what() << endl;
      return 0;
    }

  ConfigParams* cfg;
  if (parser.exists ("c"))
    {
      cfg = ConfigParams::getInstance(parser.get<string>("c"));
    }
  else
    {
      cfg = ConfigParams::getInstance();
    }

  string sizes_file, params_file, results_file, array_file, qset_file, lt_file, tr_file;
  cfg->getParam("SIZES_FILE",sizes_file);
  cfg->getParam("PARAMS_FILE",params_file);
  cfg->getParam("RESULTS_FILE",results_file);
  cfg->getParam("ARRALIST_FILE",array_file);
  cfg->getParam("QSET_FILE",qset_file);
  cfg->getParam("LOOKUPTABLE_FILE",lt_file);
  cfg->getParam("TRAPDOOR_FILE",tr_file);

  VMRSSizes vssizes;
  vssizes.importSizes (sizes_file);
  VmrsSearch vs(vssizes, params_file, array_file, lt_file, qset_file, results_file);

  if (parser.exists ("r"))
    {
      vs.set_repetitions(parser.get<int>("r"));
    }

  if (parser.exists ("a"))
    {
      vs.set_res_all_files (true);
    }

  vs.Search (tr_file);
  return 0;
}