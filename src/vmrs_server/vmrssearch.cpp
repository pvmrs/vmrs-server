#include <sstream>
#include "vmrssearch.h"


VmrsSearch::VmrsSearch (const VMRSSizes& vs, const string& pairing_file, const string& arrays_file, const string& lt_file, const string& qset_file, const string& res_file)
  : repetitions(1), verbose(true), vs(vs), res_file(res_file), start(std::chrono::high_resolution_clock::now()), res_all_files(false), lt(vs, lt_file)
{
  logmsg("Importing PBC parameters.");
  //Pairing
  FILE *sysParamFile = fopen(pairing_file.c_str(), "r");
  if (sysParamFile == nullptr) {
      cerr <<"Can't open the parameter file " << pairing_file << "\n";
      exit(1);
    }
  this->pairing = new Pairing(sysParamFile);

  logmsg("Importing ArrayList.");
  al = importArrayList (arrays_file, vs.max, vs.h1_size, vs.param_size, vs.array_size);
  logmsg("Importing QSet.");
  qs = importQSet(qset_file, vs.zp_size, vs.score_size);

  logmsg("VMRS Search intantiated.");
}

void VmrsSearch::Search (const string& file_path)
{
  ifstream ifs(file_path);
  boost::archive::text_iarchive ia(ifs);

  vector<Trapdoor> vec_tr;
  ia >> vec_tr;
  size_t vec_size = vec_tr.size();

  vector<res_list> res;
  res.resize (vec_size*repetitions);

  size_t search_count = 0;

  logmsg("Search file: " + file_path);
  for (size_t k = 0; k < vec_size; k++)
    {
      for (int j = 0; j < repetitions;j++)
        {
          //logmsg ("Start search " + to_string (k*repetitions + j));
          //logmsg("Words: " + to_string(vec_tr[k].other_w.size() + 1));
          auto search_start = high_resolution_clock::now ();

          Trapdoor * tr = &vec_tr[k];

          if (lt.find(tr->first_w) == lt.end())
            {
              //logmsg("No files with W1.");
              continue;
            }

          pLookupTable::pLookupTableItem ltItem = lt.get (tr->first_w);
          size_t files_w1 = ltItem.size;
          size_t other_size = tr->other_w.size ();

          unordered_map<unsigned long, float> result;

          //parse arraylist address from lookup table
          bitstring f_k2_w1(tr->f_k2_w1, vs.f_size);
          bitstring enc_addr(ltItem.addr, vs.f_size);
          size_t addr = (f_k2_w1^enc_addr).to_ulong ();

          //for (size_t c = 1; c <= vs.max; ++c)
          for (size_t c = 1; c <= files_w1; ++c)
            {
              ArrayData cur_ar = get_array_data (addr, c, tr->fi_k3_w1);
              //logmsg("File " + to_string(cur_ar.fid) + " found in W1, score: " + to_string(cur_ar.score) + ", " + to_hex(cur_ar.yic.toString ()) + " beta: " + to_string(cur_ar.beta));
              result[cur_ar.fid] = cur_ar.score;

              for (size_t i = 1; i <= other_size; i++)
                {
                  float cur_score=find_token (tr->other_w[i+1], tr->tokens.at({c,i+1}), cur_ar);

                  if (cur_score == 0.0 && res_all_files) {
                    result.erase(cur_ar.fid);
                    break;
                  }

                  if (cur_score > 0.0)
                    {
                      //logmsg("W=" + to_hex(tr->other_w[i+1]) + " found in File " + to_string(cur_ar.fid) + "score=" + to_string(cur_score));
                      result[cur_ar.fid] += cur_score;
                    }
                }

              //if (!cur_ar.beta) break;
            }

          res[search_count] = calculateResults(result, tr->k);

          auto search_end = high_resolution_clock::now ();

          auto search_dur = duration_cast<nanoseconds> (search_end - search_start);
          logmsg (to_string (search_count + 1) + ") Search duration " + to_string (search_dur.count ()));
          search_count++;
        }
    }

  save_results(res);
}

res_list VmrsSearch::calculateResults (const doc_list &list, int k)
{
  res_list res;

  for(const auto& [fid, score] : list)
    {
      res.push_back ({fid,score});
    }

  sort(res.begin(), res.end(), [=](std::pair<unsigned long, float>& a, std::pair<unsigned long, float>& b)
       {
           return a.second > b.second;
       }
       );

  if (res.size() > k)
    {
      res.resize (k);
    }

    //logmsg("Search found " + to_string(res.size()) + " files");


  return res;
}

void VmrsSearch::save_results (const vector<res_list>& res)
{
  ofstream ifres(this->res_file, std::ios_base::app);
  if (!ifres.good())
    {
      cout <<"Can't open the results file " << res_file << "\n";
    }

  for (size_t i = 0; i < res.size(); i++)
    {
      if (res[i].empty ())
        {
          ifres << "Search " << i+1 << ": no files found" << endl;
        }
      else
        {
          ifres << "Search " << i+1 << endl;
          for (const auto& item : res[i])
            {
              ifres << "\tFile ID: " << item.first << " Score: " << item.second << endl;
            }
        }

    }
  ifres.close ();

}

bool VmrsSearch::is_verbose () const
{
  return verbose;
}
void VmrsSearch::set_verbose (bool verbose)
{
  VmrsSearch::verbose = verbose;
}
void VmrsSearch::logmsg (const string &msg)
{
  if (verbose) {
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = duration_cast<nanoseconds>(stop - start);
      cout << duration.count() << "ns: " << msg << endl;
    }
}

int VmrsSearch::get_repetitions () const
{
  return repetitions;
}
void VmrsSearch::set_repetitions (int repetitions)
{
  VmrsSearch::repetitions = repetitions;
}

ArrayData VmrsSearch::get_array_data(size_t addr, size_t c, string& fi_k3_w1) {
  bitstring hash_h1 = vmrs::hash (fi_k3_w1 + al[addr][c-1].second, vs.h1_size);
  bitstring enc_data (al[addr][c-1].first, vs.h1_size);
  bitstring ar_data = enc_data ^hash_h1;

  ArrayData ar;
  string stryic = ar_data.substring (vs.fid_size + vs.score_size, vs.zr_size).to_string ();
  ar.yic = Zr(*pairing, (const unsigned char *)stryic.data(), bit_to_byte (vs.zr_size));
  ar.fid = ar_data.substring (vs.fid_size).to_ulong ();
  ar.score = ar_data.substring (vs.fid_size, vs.score_size).to_float ();
  ar.beta = ar_data[ar_data.size() - 1];

  return ar;
}

float VmrsSearch::find_token (string &w, string &token, ArrayData &ar) {
  G1 gtoken(*pairing, (const unsigned char *)token.data(), bit_to_byte (vs.zp_size), false, 0);
  gtoken ^= ar.yic;

  //search file c with word i in QSet
  auto found = qs.find (gtoken.toString (false));
  if (found != qs.end())
    {
      //logmsg("File " + to_string(fid) + " found in W" + to_string(i));
      bitstring enc_tfidf(found->second);
      bitstring hash_tfidf = vmrs::hash(w + to_string(ar.fid), vs.score_size);
      return (enc_tfidf^hash_tfidf).to_float ();
    }
  else
    {
      return 0.0;
    }
}
bool VmrsSearch::is_res_all_files () const
{
  return res_all_files;
}
void VmrsSearch::set_res_all_files (bool res_all_files)
{
  VmrsSearch::res_all_files = res_all_files;
}
