enable_testing()

if(NOT TARGET Catch)
    include(ExternalProject)

    ExternalProject_Add(Catch-External
            PREFIX ${CMAKE_BINARY_DIR}/external/Catch
            URL https://raw.githubusercontent.com/catchorg/Catch2/master/single_include/catch2/catch.hpp
            DOWNLOAD_NO_EXTRACT 1
            CONFIGURE_COMMAND ""
            BUILD_COMMAND ""
            INSTALL_COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/external/Catch/src/catch.hpp
            ${CMAKE_BINARY_DIR}/external/Catch/include/catch.hpp
            )
endif()


add_library(Catch INTERFACE)
add_dependencies(Catch Catch-External)
target_include_directories(Catch INTERFACE ${CMAKE_BINARY_DIR}/external/Catch/include)

add_executable(server_test server_test.cpp)
target_link_libraries(server_test PUBLIC Catch vrms_server_lib -lvrms -lpbcpp -lboost_system -lboost_filesystem -lboost_serialization -lconfig++ -lcryptopp -lpbc -lgmp)


add_custom_command(TARGET server_test POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
        ${CMAKE_SOURCE_DIR}/test/input/ $<TARGET_FILE_DIR:server_test>/input/)

#add_test(NAME TestBase COMMAND gbBase_Test)
